from splinter import Browser
from splinter.exceptions import ElementDoesNotExist
from parser.parser import crawl_nfce
import urllib.parse

def get_nfce(url):
    # executable_path = {'executable_path':'/usr/bin/phantomjs'}
    with Browser('phantomjs') as browser:
        # url = "http://nfe.sefaz.ba.gov.br/servicos/nfce/modulos/geral/NFCEC_consulta_chave_acesso.aspx?chNFe=29170847508411089203653290000453901000038336&nVersao=100&tpAmb=1&dhEmi=323031372d30382d32365431373a35343a35362d30333a3030&vNF=318.84&vICMS=17.68&digVal=5033716b37657962647a31426f515353756f682f423542657671633d&cIdToken=000001&cHashQRCode=06fb5e48c5356f4c656a659aef94a530a7c0aecd"
        browser.visit(removeSpecialCharacters(url))
        try:
            clickOnVisualizarAbas(browser)
        except ElementDoesNotExist as element:
            errorMsg = getErrorMsg(browser)
            raise Exception(errorMsg)
        else:
            nfce = crawl_nfce(browser)
            addUrlToNFCe(nfce, url)
        
        return nfce

def clickOnVisualizarAbas(browser):
    browser.find_by_xpath('//*[@id="btn_visualizar_abas"]').first.click()

def removeSpecialCharacters(url):
    return url.replace('\x1b', '')

def addUrlToNFCe(nfce, url):
    nfce['url'] = urllib.parse.unquote(removeSpecialCharacters(url))

def getErrorMsg(browser):
    return browser.find_by_xpath('//span[@id="lbl_invalido"]').first.value

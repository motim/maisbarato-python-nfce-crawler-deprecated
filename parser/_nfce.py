from lxml import html

def get_nfce(html_str):
    parsed_html = html.fromstring(html_str)

    return {
        'modelo': _get_modelo(parsed_html),
        'serie':  _get_serie(parsed_html),
        'numero': _get_numero(parsed_html),
        'data': _get_data_hora(parsed_html)[0],
        'hora': _get_data_hora(parsed_html)[1],
        'valor': _get_valor(parsed_html),
        'chave_acesso': _get_chave_acesso(parsed_html),
        'emitente' : {
            'cnpj': _get_cnpj(parsed_html),
            'razao_social': _get_razao_social(parsed_html),
            'ie': _get_ie(parsed_html),
            'uf': _get_uf(parsed_html)
        }
    }

def _get_valor(parsed_html):
    return parsed_html.xpath("//div[@id='NFe']/table[2]/tbody/tr[@class='col-6']/td[6]/span[@class='linha']/text()")[0]

def _get_modelo(parsed_html):
    return parsed_html.xpath('//*[@id="NFe"]/table[2]/tbody/tr/td[1]/span/text()')[0]

def _get_serie(parsed_html):
    return parsed_html.xpath('//*[@id="NFe"]/table[2]/tbody/tr/td[2]/span/text()')[0]

def _get_numero(parsed_html):
    return parsed_html.xpath('//*[@id="NFe"]/table[2]/tbody/tr/td[3]/span/text()')[0]

def _get_data_hora(parsed_html):
    data_hora = parsed_html.xpath('//*[@id="NFe"]/table[2]/tbody/tr/td[4]/span/text()')[0]
    return (data_hora.split(' ')[0], data_hora.split(' ')[1])

def _get_chave_acesso(parsed_html):
    return parsed_html.xpath('//*[@id="lbl_chave_acesso"]/text()')[0]

def _get_cnpj(parsed_html):
    return parsed_html.xpath('//*[@id="NFe"]/table[4]/tbody/tr/td[1]/span/text()')[0]

def _get_ie(parsed_html):
    return parsed_html.xpath('//*[@id="NFe"]/table[4]/tbody/tr/td[3]/span/text()')[0]

def _get_razao_social(parsed_html):
    return parsed_html.xpath('//*[@id="NFe"]/table[4]/tbody/tr/td[2]/span/text()')[0]

def _get_uf(parsed_html):
    return parsed_html.xpath('//*[@id="NFe"]/table[4]/tbody/tr/td[4]/span/text()')[0]
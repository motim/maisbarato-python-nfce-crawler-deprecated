from parser import _issuer
from parser import _nfce
from parser import _products

def crawl_nfce(browser):
    nfce_json = _nfce.get_nfce(browser.html);
    clickOnAbaEmitente(browser)
    issuer_json = _issuer.get_issuer(browser.html)
    nfce_json = mergeIssuerToNfce(nfce_json, issuer_json)
    clickOnAbaProdutos(browser)
    products_json = _products.get_products(browser.html)
    nfce_json = mergeProductsToNfce(nfce_json, products_json)
    return nfce_json

def mergeIssuerToNfce(nfce_json, issuer_json):
    nfce_json['emitente']['municipio'] = issuer_json['municipio']
    nfce_json['emitente']['bairro'] = issuer_json['bairro']
    nfce_json['emitente']['endereco'] = issuer_json['endereco']
    nfce_json['emitente']['cep'] = issuer_json['cep']
    nfce_json['emitente']['nome_fantasia'] = issuer_json['nome_fantasia']
    return nfce_json

def mergeProductsToNfce(nfce_json, products_json):
    nfce_json['produtos'] = products_json
    return nfce_json

def get_products(html_str):
    return _products.get_products(html_str)

def get_nfce(html_str):
    return _nfce.get_nfce(html_str)

def get_issuer(html_str):
    return _issuer.get_issuer(html_str)

def clickOnAbaEmitente(browser):
    browser.find_by_xpath('//*[@id="btn_aba_emitente"]').first.click()

def clickOnAbaProdutos(browser):
    browser.find_by_xpath('//*[@id="btn_aba_produtos"]').first.click()
FROM alpine:edge

RUN apk add --no-cache python3 \
                       python3-dev \
                       build-base \
                       git \
                       libxml2-dev \
                       libxslt-dev && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    pip3 install git+git://github.com/channelcat/sanic.git@master && \
    pip3 install splinter && \
    pip3 install lxml && \
    pip3 install Sanic-Cors && \
    apk del python3-dev \
            build-base \
            git && \
    rm -r /root/.cache && \
    apk add --update curl && \
    rm -rf /var/cache/apk/*

# RUN apk update && apk add --no-cache fontconfig && \
#         apk add tar && \
#         mkdir -p /usr/share && \
#         cd /usr/share \
#         && curl -L https://github.com/Overbryd/docker-phantomjs-alpine/releases/download/2.11/phantomjs-alpine-x86_64.tar.bz2 | tar xj \
#         && ln -s /usr/share/phantomjs/phantomjs /usr/bin/phantomjs \
#         && phantomjs --version

RUN apk update && apk add tar \
        && curl -Ls https://github.com/fgrehm/docker-phantomjs2/releases/download/v2.0.0-20150722/dockerized-phantomjs.tar.gz | tar xz

ADD . /code
WORKDIR /code
EXPOSE 5000
CMD ["python3", "main.py"]

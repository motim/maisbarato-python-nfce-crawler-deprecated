from sanic import Sanic
from sanic.response import text, json
from sanic.exceptions import ServerError
import os
import nfce_crawler as crawler
from parser import parser
from sanic_cors import CORS, cross_origin


app = Sanic(__name__)
CORS(app)

@app.route("/nfce", methods=['GET'])
async def nfce(request):
    if 'url' not in request.args:
        raise ServerError('No "url" parameter was found.', status_code=422)
    try:
        nfceData = crawler.get_nfce(request.args['url'][0])
        return json(nfceData)
    except Exception as e:
        raise ServerError(str(e), status_code=404)

@app.route("/products", methods=['POST'])
async def products(request):
    return json(parser.get_products(request.json['html_string']))

@app.route("/cupons", methods=['POST'])
async def products(request):
    return json(parser.get_nfce(request.json['html_string']))

@app.route("/issuers", methods=['POST'])
async def products(request):
    return json(parser.get_issuer(request.json['html_string']))

@app.route("/")
async def root(request):
    return text('Up and running')


if __name__ == '__main__':
    port = int(os.environ.get("PORT", 5000))
    app.run(host="0.0.0.0", port=port, debug=False, workers=2)
